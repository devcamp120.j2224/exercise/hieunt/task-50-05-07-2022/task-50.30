
public class App {
    public static void main(String[] args) throws Exception {
        // in báo hiệu chạy app main
        System.out.println("CHAO MUNG DEN VOI DEVCAMP 120!"); 
        /*
         * Đây là khối comment
         */
        String appName = "Quyet tam thanh lap trinh vien!"; // tạo biến String
        System.out.println(appName); // in ra biến String
        System.out.println("String có length là " + appName.length()); // ghi ra độ dài string
        System.out.println("String có Upper Case là " + appName.toUpperCase()); // ghi ra chữ in to
        System.out.println("String có Lower Case là " + appName.toLowerCase()); // ghi ra chữ in nhỏ
    }
}
